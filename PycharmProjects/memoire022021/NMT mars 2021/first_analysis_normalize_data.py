import re
import string
from collections import Counter
from unicodedata import normalize
from pickle import dump
"""
These functions' are :
    - load the doc and read each line 
    - split to sentences by \n separator 
    - Count the sentences' length - Min and max
    - put character into its normal form with 
    
"""
def load_doc(filename):
    #open the file for read only
    file = open(filename,mode='rt',encoding='utf-8')
    text = file.read()
    file.close()
    #original raw text
    return text

#split text into sentences
def to_sentences(doc):
    #remove trailing chars
    return doc.strip().split('\n')
#show sortest and longest sentences
def sentence_lengths(sentences):
    #get the length of each seantence
    lengths = [len(s.split()) for s in sentences]
    #the min max length of s
    return min(lengths), max(lengths)

def clean_lines(lines):
    #stock lines in static unchanged list
	cleaned = list()
	# prepare regex for char filtering
	re_print = re.compile('[^%s]' % re.escape(string.printable))
	# prepare translation table for removing punctuation
	table = str.maketrans('', '', string.punctuation)
	for line in lines:
		# normalize unicode characters
		line = normalize('NFD', line).encode('utf-8', 'ignore')
		line = line.decode('UTF-8')
		# tokenize on white space
		line = line.split()
		# convert to lower case
		line = [word.lower() for word in line]
		# remove punctuation from each token
		line = [word.translate(table) for word in line]
		# remove non-printable chars form each token
		line = [re_print.sub('', w) for w in line]
		# remove tokens with numbers in them
		line = [word for word in line if word.isalpha()]
		# store as string
		cleaned.append(' '.join(line))
	return cleaned


# save a list of clean sentences to file
def save_clean_sentences(sentences, filename):
	dump(sentences, open(filename, 'wb'))
	print('Saved: %s' % filename)

# load English data
filename = 'english'
doc = load_doc(filename)
sentences = to_sentences(doc)
sentences = clean_lines(sentences)
save_clean_sentences(sentences, 'english.pkl')
#spot check
# for i in range(10):
#     print(sentences[i])
#load French data
filename = 'french'
doc = load_doc(filename)
sentences = to_sentences(doc)
sentences = clean_lines(sentences)
save_clean_sentences(sentences, 'french.pkl')
# spot check
# for i in range(20):
#     print(sentences[i])

""" REopen the En.pkl  and fr.pkl to Reduce VOCABULARY """

def load_clean_sentences(filename):
	return load_doc(open(filename,'rb'))

def to_vocab(lines):
	vocab = Counter()
	for line in lines:
		tokens = line.split()
		vocab.update(tokens)
	return vocab
"""The trim_vocab() function below does this and accepts a minimum occurrence 
count as a parameter and returns an updated vocabulary."""
def trim_vocab(vocab, min_occurance):
	tokens = [k for k,c in vocab.items() if c >= min_occurance]
	return set(tokens)

# mark all OOV with "unk" for all lines
def update_dataset(lines, vocab):
	new_lines = list()
	for line in lines:
		new_tokens = list()
		for token in line.split():
			if token in vocab:
				new_tokens.append(token)
			else:
				new_tokens.append('unk')
		new_line = ' '.join(new_tokens)
		new_lines.append(new_line)
	return new_lines


filename = 'english.txt' #pkl to txt
lines = load_clean_sentences(filename)
# calculate vocabulary
vocab = to_vocab(lines)
print('English Vocabulary: %d' % len(vocab))
# reduce vocabulary
vocab = trim_vocab(vocab, 5)
print('New English Vocabulary: %d' % len(vocab))
# mark out of vocabulary words
lines = update_dataset(lines, vocab)
# save updated dataset
filename = 'enCorpus.txt'
save_clean_sentences(lines, filename)
# spot check

for i in range(10):
	print(lines[i])

# load French dataset
filename = 'french.txt' #pkl to txt
lines = load_clean_sentences(filename)
# calculate vocabulary
vocab = to_vocab(lines)
print('French Vocabulary: %d' % len(vocab))
# reduce vocabulary
vocab = trim_vocab(vocab, 5)
print('New French Vocabulary: %d' % len(vocab))
# mark out of vocabulary words
lines = update_dataset(lines, vocab)
# save updated dataset
filename = 'frCorpus.txt' #pkl to txt
save_clean_sentences(lines, filename)
# spot check
for i in range(10):
	print(lines[i])

