# Neural Machine Translation *tf Seq_2_seq* of Medical Multiword Expressions

This is an end-to-end neural machine translation system (NMT) of mutiword expressions (MWE) including :

- **explore_EMEA_corpus** Cleaning the corpus EMEA (European Medicines Agency) then explore its bilingual aligned features. These are english to french translation of medical texts and phrases.

* Explore the EMEA corpus in terms of features frequency and length. 
    In other words, we form a unique darafraame for each expression length. 
    To be precise, bigram_df is a dataframe of bigrams, etc.
* Cleaning the corpus from NAN, duplicate values. 


- **data_revision**, in the second point, point out a deeper analysis, basically, 
* extracting the features on the basis of the features length and frequency and replacing some mispelled features. 
* Also, you can visualize some features' correlation, we find that features are highly correlated with a score of 1. 

**identify_POS_patterns** : we annotate the expressions with spacy trained models. We find out that the dataset linguistic features are mainly :

    * Compound Nouns or nominal groups, such as Change in baseline.  Modification de la base de référence.
    * Named Entities,  such as  Ireland Boehringer Ingelheim Ireland Ltd
    * Collocations, these are frequent association from one word to another within a sentence (https://fr.wikipedia.org/wiki/Collocation_(linguistique)).
    * Sentences in question, affirmative, negative and imperative forms: Why has Lantus been approved ?  Pourquoi Lantus a t'il été approuvé ?

- Model training and testing starting with simple keras encoder-decoder LSTM architecture. In the second part, we use a backend tensorflow model _2.3_ with an attention layer. 

- Model Evaluation :
    * particular focus on loss values during testing and training.
    * counting the BLEU score of the baseline. The latter is trained on a corpus of 8000 expressions and tested on 2000 expressions. the generated score is 25.

    * In fact the corpus is not that large but in a second evaluation with a 32000 expression the BLEU score is fairly increased to 28.
I was inspired directly by the tensorflow API, particularly the inference model : 
(https://www.tensorflow.org/tutorials/text), nmt_with_attention and one of the kaggle works (https://www.kaggle.com/).

For other options, for example, looking for expressions of some length; let's say compound nouns bigrams and trigrams you can simply do :

    * First count the lines length in the table with :
        `en_count = df.en.str.split().str.len()
         fr_count = df.fr.str.split().str.len()`

    * Then apply a **boolean mask** of the expression that you're looking for :
        `df = df[~(en_count >= 4)]` 

This will be done after reading the dataframe object and cleaning the corpus *exploratory_emea_corpus* as explained also in the *form_the_dataset*.

## References

* Ratchel Tatman. 2019. Evaluating Text Output in NLP: BLEU at your own risk (https://towardsdatascience.com/evaluating-text-output-in-nlp-bleu-at-your-own-risk-e8609665a213).
* Dzmitry Bahdanau, Kyunghyun Cho, and Yoshua Bengio. 2015. Neural machine translation by jointly learning to align and translate. ICLR.
* Ilya Sutskever, Oriol Vinyals, and Quoc V. Le. 2014. Sequence to sequence learning with neural networks. NIPS.



